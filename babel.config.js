/*
 * Author: zhanghj
 * Date: 2020-08-13 15:06:08
 * LastEditors: 黄荣基
 * LastEditTime: 2020-12-01 15:48:33
 */

module.exports = {
  presets: ['@vue/cli-plugin-babel/preset'],
  plugins: [
    [
      'component',
      {
        libraryName: 'element-ui',
        styleLibraryName: 'theme-chalk'
      }
    ]
  ]
};
