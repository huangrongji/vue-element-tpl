const path = require('path');

module.exports = {
  publicPath: './',
  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'zh',
      localeDir: 'lang',
      enableInSFC: true
    }
  },
  chainWebpack: config => {
    // 修复HMR
    config.resolve.symlinks(true);
  },
  css: {
    // extract: false
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
        img: path.resolve(__dirname, 'src/assets/images')
      }
    }
  },
  devServer: {
    port: 9090,
    open: true
    // proxy: {

    // }
  }
};
