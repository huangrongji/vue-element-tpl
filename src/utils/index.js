/* eslint-disable one-var */
// date对象 => 时间戳
export const dateTimeToTimeStamp = date => {
  return new Date(date).getTime();
};
//  获取ISO 8601时间
export const isoStrTime = date => {
  date.setHours(date.getHours(), date.getMinutes() - date.getTimezoneOffset());
  return date.toISOString();
};
/**
 * 获取年份
 */
export const getYear = () => {
  return new Date().getFullYear();
};

/**
 * 获取当前月份
 * @param {Boolean} fillFlag 布尔值,是否补 0,默认为 true
 */
export const getMonth = (fillFlag = true) => {
  const mon = new Date().getMonth() + 1;
  const monRe = mon;
  if (fillFlag) mon < 10 ? `0${mon}` : mon;
  return monRe;
};

/**
 * 获取日
 * @param {Boolean} fillFlag 布尔值,是否补 0
 */
export const getDay = (fillFlag = true) => {
  const day = new Date().getDate();
  const dayRe = day;
  if (fillFlag) day < 10 ? `0${day}` : day;
  return dayRe;
};

/**
 * 获取星期几
 */
export const getWhatDay = () => {
  return new Date().getDay() ? new Date().getDay() : 7;
};

/**
 * 根据日期字符串获取星期几
 * @param dateString 日期字符串（如：2020-05-02）
 * @returns {String}
 */
export const getWeek = dateString => {
  const dateArray = dateString.split('-');
  const date = new Date(dateArray[0], parseInt(dateArray[1] - 1), dateArray[2]);
  return `周${'日一二三四五六'.charAt(date.getDay())}`;
};

/**
 * 获取当前月天数
 * @param {String} year 年份
 * @param {String} month 月份
 */
export const getMonthNum = (year, month) => {
  const d = new Date(year, month, 0);
  return d.getDate();
};

/**
 * 获取当前时间 yyyy-mm-dd,hh:mm:ss
 */
export const getYyMmDdHhMmSs = () => {
  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hours = date.getHours();
  const minu = date.getMinutes();
  const second = date.getSeconds();
  const arr = [month, day, hours, minu, second];
  arr.forEach((item, index) => {
    arr[index] = item < 10 ? `0${item}` : item;
  });
  return `${year}-${arr[0]}-${arr[1]} ${arr[2]}:${arr[3]}:${arr[4]}`;
};
