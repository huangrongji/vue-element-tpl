/*
 * Author: 黄荣基
 * Date: 2020-12-01 15:02:54
 * LastEditors: 黄荣基
 * LastEditTime: 2020-12-01 15:53:21
 */
/* eslint-disable one-var */
/* eslint-disable prefer-spread */
/* eslint-disable prefer-destructuring */
// ./src/mixins/emitter.js
export default {
  methods: {
    dispatch(componentName, eventName, params) {
      let parent = this.$parent || this.$root;
      let { name } = parent.$options;
      while (parent && (!name || name !== componentName)) {
        parent = parent.$parent;
        if (parent) name = parent.$options.name;
      }
      if (parent) parent.$emit.apply(parent, [eventName].concat(params));
    }
  }
};
