import request from '@/utils/request';
// import qs from "qs"

// const serverUrl = process.env.VUE_APP_BASE_API,

const demo = {
  // GET请求
  list(params) {
    return request.get(
      'https://api.baiyunairport.top/airport-business/listForWechat?openId=null&airportCode=CAN&terminal=&out=&beforeSecurity=&type=2&businessName=&page=1&rows=10',
      { params }
    );
  },

  // POST请求
  weatherInfo(params) {
    return request.post(
      'https://api.baiyunairport.top/flight-center/common/getWeatherInfo',
      params
    );
  }
};

export default demo;
